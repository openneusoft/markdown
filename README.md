# Markdown

> 鸿蒙平台的原生Markdown解析器

* 由markdown文本直接转换为Html，快捷高效
* 不依赖特定控件，低侵入性
* 遵循 CommonMark规范


#### 项目介绍

- 项目名称：一个Markdown文本解析器
- 所属系列：ohos的第三方组件适配移植
- 功能：是一种轻量级**标记语言**，它以纯文本形式(_易读、易写、易更改_)编写文档
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/zzhoujay/RichText
- 原项目基线版本：v1.0.5
- 编程语言：Java 
- 外部库依赖：无

### 效果展示

<img src="image0/img1.jpg"/>
<img src="image0/img2.jpg"/>
<img src="image0/img3.jpg"/>
<img src="image0/img4.jpg"/>

#### 安装教程

方法1.

1. 编译har包Markdown.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
	maven {
		url 'https://s01.oss.sonatype.org/content/repositories/releases/'
	}
}

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
	implementation 'io.github.dzsf:markdown:1.0.0'
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

#### 版本迭代

- v1.0.0


#### License

```
The MIT License (MIT)

Copyright (c) 2015 zzhoujay

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR

IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
