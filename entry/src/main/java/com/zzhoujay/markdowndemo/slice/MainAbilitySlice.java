/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import com.zzhoujay.markdowndemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.webengine.WebView;
import ohos.global.resource.RawFileEntry;

import java.io.IOException;
import java.io.InputStream;

public final class MainAbilitySlice extends AbilitySlice {

    /* (non-Javadoc)
     * @see MainAbilitySlice#onStart()
     *
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        WebView webView = (WebView) findComponentById(ResourceTable.Id_webView);
        RawFileEntry rawFileEntry = getResourceManager().getRawFileEntry("resources/rawfile/test.md");
        InputStream snputStream = null;

        try {
            snputStream = rawFileEntry.openRawFile();
            webView.load(MarkDown.fromMarkdown(snputStream), "text/html", false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see MainAbilitySlice#onActive()
     *
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /* (non-Javadoc)
     * @see MainAbilitySlice#onForeground()
     *
     * @param intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
