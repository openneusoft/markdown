package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestDivdingLine {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"dividingLine1.md","dividingLine2.md","dividingLine3.md","dividingLine4.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "dividingLine1.md":
                    assertStr = "<p>---这是一个分割线</p>\n"+"";
                    break;
                case "dividingLine2.md":
                    assertStr = "<p>----这是第二个分割线</p>\n"+"";
                    break;
                case "dividingLine3.md":
                    assertStr = "<p>***这是第三个分割线</p>\n"+"";
                    break;
                case "dividingLine4.md":
                    assertStr = "<p>****这是第四个分割线</p>\n"+"";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/dividingLine/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}