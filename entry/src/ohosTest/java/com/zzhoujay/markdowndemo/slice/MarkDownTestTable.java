package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestTable {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"table1.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "table1.md":
                    assertStr = "<p>|代码库                              |链接                                |</p>\n" +
                            "<p>|:------------------------------------:|------------------------------------|</p>\n" +
                            "<p>|MarkDown                              |<a href=\"https://github.com/younghz/Markdown\" title=\"Markdown\" target=\"_blank\">https://github.com/younghz/Markdown</a>|</p>\n" +
                            "<p>|moos-young                            |<a href=\"https://github.com/younghz/moos-young\" title=\"tianchi\" target=\"_blank\">https://github.com/younghz/moos-young</a>|</p>" +
                            "\n"+"";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/table/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}