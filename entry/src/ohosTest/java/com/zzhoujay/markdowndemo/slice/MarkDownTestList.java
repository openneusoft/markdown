package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestList {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"list1.md","list2.md","list3.md","list4.md","list5.md","list6.md","list7.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "list1.md":
                    assertStr = "<ul>\n<li>列表内容，无序列表1</li>\n</ul>\n";
                    break;
                case "list2.md":
                    assertStr = "<ul>\n<li>列表内容，无序列表2</li>\n</ul>\n";
                    break;
                case "list3.md":
                    assertStr = "<ul>\n<li>列表内容，无序列表3</li>\n</ul>\n";
                    break;
                case "list4.md":
                    assertStr = "<ol>\n<li>列表内容，有序列表1</li>\n</ol>\n";
                    break;
                case "list5.md":
                    assertStr = "<ol start=\"2\">\n<li>列表内容，有序列表2</li>\n</ol>\n";
                    break;
                case "list6.md":
                    assertStr = "<ol start=\"3\">\n<li>列表内容，有序列表3</li>\n</ol>\n";
                    break;
                case "list7.md":
                    assertStr = "<ol>\n" +
                            "<li>第一项：</li>\n" +
                            "</ol>\n" +
                            "<pre><code>- 第一项嵌套的第一个元素\n" +
                            "</code></pre>\n" +
                            "<pre><code>- 第一项嵌套的第二个元素\n" +
                            "</code></pre>\n" +
                            "<ol start=\"2\">\n" +
                            "<li>第二项：</li>\n" +
                            "</ol>\n" +
                            "<pre><code>- 第二项嵌套的第一个元素\n" +
                            "</code></pre>\n" +
                            "<pre><code>- 第二项嵌套的第二个元素\n" +
                            "</code></pre>\n";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/list/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}