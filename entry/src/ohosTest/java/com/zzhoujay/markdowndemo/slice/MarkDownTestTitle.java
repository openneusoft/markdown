package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestTitle {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"h1.md","h2.md","h3.md","h4.md","h5.md","h6.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "h1.md":
                    assertStr = "<h1 id=\"这是一级标题\">这是一级标题</h1>\n";
                    break;
                case "h2.md":
                    assertStr = "<h2 id=\"这是二级标题\">这是二级标题</h2>\n";
                    break;
                case "h3.md":
                    assertStr = "<h3 id=\"这是三级标题\">这是三级标题</h3>\n";
                    break;
                case "h4.md":
                    assertStr = "<h4 id=\"这是四级标题\">这是四级标题</h4>\n";
                    break;
                case "h5.md":
                    assertStr = "<h5 id=\"这是五级标题\">这是五级标题</h5>\n";
                    break;
                case "h6.md":
                    assertStr = "<h6 id=\"这是六级标题\">这是六级标题</h6>\n";
                    break;
                case "link.md":
                    assertStr = "<a href=\"http://www.baidu.com\">baidu</a>";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/hFile/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}