package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestParagraph {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"paragraph1.md","paragraph2.md","paragraph3.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "paragraph1.md":
                    assertStr = "<p>每一个输出的HOS日志信息都有一个标签和它的优先级.</p>\n" +
                            "<p>###过滤日志输出</p>\n";
                    break;
                case "paragraph2.md":
                    assertStr = "<p>第一行(行末2个空格)</p>\n" +
                            "<p>第二行</p>\n";
                    break;
                case "paragraph3.md":
                    assertStr = "<p>　　段落，</p>\n" +
                            "<p>(首行缩进)</p>\n";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/paragraph/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}