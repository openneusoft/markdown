package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestTypeface {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"typefaceFile1.md","typefaceFile2.md","typefaceFile3.md","typefaceFile4.md","typefaceFile5.md"
                ,"typefaceFile6.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "typefaceFile1.md":
                    assertStr = "<p><strong>这是加粗的文字</strong></p>\n"+"";
                    break;
                case "typefaceFile2.md":
                    assertStr = "<p><em>这是倾斜的文字</em></p>\n"+"";
                    break;
                case "typefaceFile3.md":
                    assertStr = "<p><em><strong>这是斜体加粗的文字</strong></em></p>\n"+"";
                    break;
                case "typefaceFile4.md":
                    assertStr = "<p><del>这是加删除线的文字</del></p>\n"+"";
                    break;
                case "typefaceFile5.md":
                    assertStr = "<p><em>这是斜体的文字2</em></p>\n"+"";
                    break;
                case "typefaceFile6.md":
                    assertStr = "<p><strong>这是加粗的文字2</strong></p>\n"+"";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/typefaceFile/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}