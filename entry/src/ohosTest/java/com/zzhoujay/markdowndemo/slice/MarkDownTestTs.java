package com.zzhoujay.markdowndemo.slice;

import com.zzhoujay.markdown.MarkDown;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MarkDownTestTs {

    @Test
    public void fromMarkdown() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String[] mdFileNames = {"Ts1.md","Ts2.md","Ts3.md"};
        for (int i = 0; i < mdFileNames.length; i++) {
            String markDownStr = fromMarkdownTest(context, mdFileNames[i]);
            String assertStr = "";
            switch (mdFileNames[i]) {
                case "Ts1.md":
                    assertStr = "<p>(1)aapt</p>\n" +
                            "<p>（Hos Asset Package Tool）</p>\n" +
                            "<p>Hos资源打包工具</p>\n" +
                            "<p>${HOS_SDK_HOME} /build-tools/</p>\n" +
                            "<p>HOS_VERSION/aapt</p>\n" +
                            "<p>frameworks\\base\\tools\\aap\\</p>\n" +"";
                    break;
                case "Ts2.md":
                    assertStr = "<pre><code class=\"language-py\"></code></pre>\n" +
                            "<p>print $0</p>\n<pre><code></code></pre>\n"+"";
                    break;
                case "Ts3.md":
                    assertStr = "<h5 id=\"zb\">zb</h5>\n" +
                            "<p>#####hb#####</p>\n";
                    break;
                default:
                    break;
            }
            assertEquals(assertStr, markDownStr);
        }
    }

    private String fromMarkdownTest(Context context, String mdFileName) throws IOException {
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/Ts/" + mdFileName);
        return MarkDown.fromMarkdown(rawFileEntry.openRawFile());
    }
}